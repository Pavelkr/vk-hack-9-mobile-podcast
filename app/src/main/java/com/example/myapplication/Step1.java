package com.example.myapplication;

import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Step1 extends AppCompatActivity {
    Button uploadFileButton;
    Uri selectedfile;
    LinearLayout audioSelected;
    TextView audioName;
    TextView audioLenght;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step_1);

         /*
        Поменяем заголовок
         */
        getSupportActionBar().setTitle("Новый подкаст");

        /*
         * Активируем кнопку назад
         * */
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        uploadFileButton = findViewById(R.id.uploadFileButton);

        uploadFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent()
                        .setType("audio/*")
                        .setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Выберите подкаст"), 777);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 777 && resultCode == RESULT_OK) {
            selectedfile = data.getData();

            if (selectedfile != null) {
                // load data file
                /*Тут должен был быть выбор файла и получение его утрибуто, но не шмог :(*/
                /*String out;
                try (MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever()) {
                    metaRetriever.setDataSource("content://com.android.providers.downloads.documents/document/raw%3A%2Fstorage%2Femulated%2F0%2FDownload%2F%D0%91%D0%B0%D1%81%D1%82%D0%B0%20feat.%20Zivert%20-%20%D0%9D%D0%B5%D0%B1%D0%BE%D0%BB%D0%B5%D0%B9.mp3");

                    out = "";
                    // get mp3 info

                    // convert duration to minute:seconds
                    String duration =
                            metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                    long dur = Long.parseLong(duration);
                    String seconds = String.valueOf((dur % 60000) / 1000);
                    String minutes = String.valueOf(dur / 60000);
                    out = minutes + ":" + seconds;

                    metaRetriever.release();
                }*/


                RelativeLayout uploadTrack;
                RelativeLayout audioSelected;

                uploadTrack = findViewById(R.id.uploadTrack);
                uploadTrack.setVisibility(View.GONE);

                audioSelected = findViewById(R.id.audioSelected);
                audioSelected.setVisibility(View.VISIBLE);

                audioName = findViewById(R.id.audioName);
                audioLenght = findViewById(R.id.audioLenght);

                //audioName.setText(out);
            }
        }
    }



    /*
    Обраотка кнопки назад
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}